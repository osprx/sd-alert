# SD-Alert

## SD-Alert produces Warnings (Tone and Alert Message) if Removable Storage(s) are not removed after backup.
  
---
**Description:**  
  
sd-alert.sh generates speaker alert tone and notify message on desktop if an 
SD-Card or other storage with one of the predefined '.id' files (semaphore
file in the root of the Mounted Partition) connected.

The point of this idea is to prevent forgetting the storage plugged, 
for example after completed backups.
The alarms are generated in a random time range between 40 and 90 seconds 
to avoid monotony and are short sound sequences to not disturb anyone.
The .id file is searched for and recognized on all mounted drives.
So you can mount an external storage medium on different systems, 
the medium is always recognized as the same.  

---
**Instructions:**  

* Copy the shell script 'sd-alert.sh' and the config file 'sd-alert.cfg' to the /bin directory as root and make 'sd-alert.sh' executable.
* Create on each removable storage in the root of the drive a semaphore file (for example with the extension *.id).
* Define the Variables ID0..IDx in the config file (sd-alert.cfg) according to your requirements.
* Run the script in the Background (invisible terminal window) at system start with 'sd-alert.sh &' or from running Terminal session with 'nohup sd-alert.sh &'.

On systems without grapical desktop environment or for testing purposes you can run the script in a visible terminal window for watching echo outputs with 'sd-alert.sh' without any options. 

---
**Comments:**

reasons for using of semaphore files (.id) to detect connected removable media instead of hardware detection (of mounted devices):
 
* More Usability: Can be easier used because don't need any hardware access with sudo privileges.
* The .id files can be used on different Machines with different Mountpoints (The same storage media can be mounted with different Mountpoints in different environments as pc/laptop/server). The Script always recognizes the storage based on the id-file. 
* More Flexibility: with changing the storage medium it is not necessary to detect new hardware id or other medium properties, the script can be automated easier.
* More Reliability: in Virtual Machines (VMs) works the script more steady, there are no hardware detection problems

The script recognizes the environment in which it is running (vm/desktop/laptop) and emits different sounds depending on the environment 

---
    
**System Requirements:**

* installed ffmpeg package

Tested on Bash Version: 5.0.17(1)-release (x86_64-pc-linux-gnu)

---

**Limitations:**

no detection if the storage medium is dismounted. It is necessary to remove any storage medium immediately after dismounting it.

---
Known issues:
	-
	
---
**List of references:**

https://askubuntu.com/questions/88091/how-to-run-a-shell-script-in-background
https://stackoverflow.com/questions/1921279/how-to-get-a-variable-value-if-variable-name-is-stored-as-string
https://unix.stackexchange.com/questions/82112/stereo-tone-generator-for-linux
https://www.systutorials.com/docs/linux/man/1-speaker-test/
https://unix.stackexchange.com/questions/1974/how-do-i-make-my-pc-speaker-beep
https://stackoverflow.com/questions/2556190/random-number-from-a-range-in-a-bash-script
https://misc.flogisoft.com/bash/tip_colors_and_formatting
https://ostechnix.com/check-linux-system-physical-virtual-machine/

---
